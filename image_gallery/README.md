# Image Gallery Module

A standalone module which can be used to display web hosted images in Gallery (Grid and List). User can click any image to open it in full screen(ImageViewer) mode.

ImageViewer mode has following features.

- Zoom and Pan Image.
- Share Image with any sharable applications installed in device i.e. (Instagram, WhatsApp, Gmail...)
- Save Image in Device Local Gallery.

## How to use

### Import Module in Project

Open pubspec.yaml file and add below lines under dependencies.

```dart
image_gallery:
    path: '<path>image_gallery'
```

### Use GalleryMain Widget

```dart
// Directly use gallery for picsum images
GalleryMain()

// All the parameters for Gallery
GalleryMain(
        infiniteScroll: true, // optional (Default value = true)
        initialImageCount: 500, // optional (Default value = 300)
        title: "Demo Gallery", // optional (Default value = "Gallery)
        imageUrls: const [   // optional (Default picsum photos)
          "https://picsum.photos/200?image=222",
          "https://picsum.photos/200?image=241",
          "https://picsum.photos/200?image=141",
          "https://picsum.photos/200?image=341",
          "https://picsum.photos/200?image=512",
        ],
      )
```
