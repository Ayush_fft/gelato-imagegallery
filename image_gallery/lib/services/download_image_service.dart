import 'dart:typed_data';

import 'package:dartz/dartz.dart';
import 'package:http/http.dart' show get;
import 'failure.dart';

abstract class DownloadImageService {
  Future<Either<Failure, Uint8List>> downloadImage(String imageUrl);
}

class DownloadImageServiceImpl extends DownloadImageService {
  @override
  Future<Either<Failure, Uint8List>> downloadImage(String imageUrl) async {
    final response = await get(Uri.parse(imageUrl));

    return response.statusCode == 200
        ? Right(response.bodyBytes)
        : Left(ServerFailure(
            response.body,
            response.statusCode,
          ));
  }
}
