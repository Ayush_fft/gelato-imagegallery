import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_gallery/di/initial_bindings.dart';

import 'navigation/routes.dart';
import 'pages/gallery_page.dart';
import 'utils/size_utils.dart';

class GalleryMain extends StatelessWidget {
  const GalleryMain._({
    Key? key,
    required this.title,
    required this.imageUrls,
    required this.infiniteScroll,
    required this.initialImageCount,
  }) : super(key: key);

  final String title;
  final List<String>? imageUrls;
  final bool infiniteScroll;
  final int? initialImageCount;

  factory GalleryMain({
    Key? key,
    String? title,
    List<String>? imageUrls,
    bool? infiniteScroll,
    int? initialImageCount,
  }) {
    title = title ?? "Gallery";
    infiniteScroll = infiniteScroll ?? true;
    if (initialImageCount != null && initialImageCount > 1000) {
      throw ("Initial image count cannot be greater than 1000");
    }

    return GalleryMain._(
      key: key,
      title: title,
      imageUrls: imageUrls,
      infiniteScroll: infiniteScroll,
      initialImageCount: initialImageCount,
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      builder: (context, child) {
        initSizeUtils();

        return child!;
      },
      getPages: Routes.routes,
      debugShowCheckedModeBanner: false,
      initialBinding: InitialBindings(),
      home: GalleryPage(
        infiniteScroll: infiniteScroll,
        initialImageCount: initialImageCount,
        title: title,
        imageUrls: imageUrls,
      ),
    );
  }
}
