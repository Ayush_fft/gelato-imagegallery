import 'package:image_gallery/utils/size_utils.dart';

const portraitGridCount = 3;
const landscapeGridCount = 5;
const portraitListCount = 1;
const gridImageSize = 200;
const listImageSize = 400;
const gridAspectRatio = 1.0;
const listAspectRatio = 2.0;
const fullScreenImageSize = 1000;
final gridSpace = 2.iX;
final toolbarActionSpace = 18.iX;
