import 'package:get/get.dart';
import 'package:image_gallery/utils/colors.dart';

void showSnackBar(String message) {
  Get.snackbar(
    "Alert!",
    message,
    colorText: CustomColors().snackBarTextColor,
    snackPosition: SnackPosition.BOTTOM,
    duration: const Duration(seconds: 1),
    animationDuration: const Duration(milliseconds: 200),
  );
}
