import 'dart:ui';

class CustomColors {
  final Color toolbarColor = const Color(0xFF212121);
  final Color scaffoldColor = const Color(0xFF424242);
  final Color shimmerBaseColor = const Color(0x8AFFFFFF);
  final Color shimmerHighlightColor = const Color(0x8A000000);
  final Color snackBarTextColor = const Color(0xFFFFFFFF);

  static final CustomColors _instance = CustomColors._();

  factory CustomColors() {
    return _instance;
  }

  CustomColors._();
}
