import 'package:flutter/material.dart';
import 'package:get/get.dart';

const _refH = 1075.2;
const _refW = 496.8;
// 1440x3040
late _SizeUtils _sizeUtilsInstance;

// ignore: non_constant_identifier_names
_SizeUtils get SizeUtils => _sizeUtilsInstance;

void initSizeUtils() {
  _sizeUtilsInstance = _SizeUtils.internal();
}

class _SizeUtils {
  final Size _screenSize;

  _SizeUtils.internal() : _screenSize = Get.size;

  Size get screenSize => _screenSize;

  double get height => _screenSize.height;

  double get width => _screenSize.width;

  double iX(double refVal) => width * (refVal / _refW);

  double iY(double refVal) => height * (refVal / _refH);

  double fractionX(double refVal) => width * refVal;

  double fractionY(double refVal) => height * refVal;

  Size getTextSize(String text, TextStyle style) => (TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textScaleFactor: Get.textScaleFactor,
        textDirection: TextDirection.ltr,
      )..layout())
          .size;
}

extension IntSizeExtension on int {
  double get iX => SizeUtils.iX(toDouble());

  double get iY => SizeUtils.iY(toDouble());

  double get fractionX => SizeUtils.fractionX(toDouble());

  double get fractionY => SizeUtils.fractionY(toDouble());
}

extension DoubleSizeExtension on double {
  double get iX => SizeUtils.iX(this);

  double get iY => SizeUtils.iY(this);

  double get fractionX => SizeUtils.fractionX(this);

  double get fractionY => SizeUtils.fractionY(this);
}
