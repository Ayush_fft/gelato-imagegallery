import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery/utils/colors.dart';
import 'package:shimmer/shimmer.dart';
import 'package:image_gallery/utils/size_utils.dart';

class GalleryImageWidget extends StatelessWidget {
  const GalleryImageWidget._({
    Key? key,
    required this.imageUrl,
    required this.itemClickCallback,
  }) : super(key: key);

  factory GalleryImageWidget({
    Key? key,
    required String imageUrl,
    required VoidCallback itemClick,
  }) {
    return GalleryImageWidget._(
      key: key,
      imageUrl: imageUrl,
      itemClickCallback: itemClick,
    );
  }

  final String imageUrl;
  final VoidCallback itemClickCallback;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: GestureDetector(
        onTap: itemClickCallback,
        child: CachedNetworkImage(
          fit: BoxFit.cover,
          placeholder: (context, url) => Shimmer.fromColors(
            baseColor: CustomColors().shimmerBaseColor,
            highlightColor: CustomColors().shimmerHighlightColor,
            child: Center(
              child: Icon(
                Icons.image,
                size: 100.iY,
              ),
            ),
          ),
          imageUrl: imageUrl,
          errorWidget: (context, url, error) => Center(
            child: Icon(
              Icons.broken_image,
              size: 100.iY,
              color: CustomColors().shimmerBaseColor,
            ),
          ),
        ),
      ),
    );
  }
}
