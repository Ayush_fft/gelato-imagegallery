import 'package:flutter/material.dart';
import 'package:image_gallery/utils/colors.dart';
import 'package:image_gallery/utils/dimens.dart';

class GalleryAppBar extends AppBar {
  GalleryAppBar({
    Key? key,
    required String title,
    List<Widget>? actions,
  }) : super(
          key: key,
          backgroundColor: CustomColors().toolbarColor,
          centerTitle: true,
          title: Text(title),
          actions: actions
              ?.map((e) => Container(
                    margin: EdgeInsets.only(right: toolbarActionSpace),
                    child: e,
                  ))
              .toList(),
        );
}
