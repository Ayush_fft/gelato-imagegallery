import 'package:flutter/material.dart';
import 'package:image_gallery/utils/colors.dart';
import 'package:image_gallery/utils/dimens.dart';
import 'package:image_gallery/widgets/app_bar_widget.dart';
import 'package:image_gallery/widgets/gallery_image_widget.dart';
import 'package:image_gallery/controllers/gallery_page_controller.dart';
import 'package:image_gallery/pages/image_viewer_page.dart';
import 'package:get/get.dart';

class GalleryPage extends StatefulWidget {
  const GalleryPage._({
    Key? key,
    required this.title,
    required this.imageUrls,
    required this.infiniteScroll,
    required this.initialImageCount,
  }) : super(key: key);

  final String title;
  final List<String>? imageUrls;
  final bool infiniteScroll;
  final int? initialImageCount;

  factory GalleryPage({
    Key? key,
    String? title,
    List<String>? imageUrls,
    bool? infiniteScroll,
    int? initialImageCount,
  }) {
    title = title ?? "Gallery";
    infiniteScroll = infiniteScroll ?? true;
    if (initialImageCount != null && initialImageCount > 1000) {
      throw ("Demo image count cannot be greater than 1000");
    }

    return GalleryPage._(
      key: key,
      title: title,
      imageUrls: imageUrls,
      infiniteScroll: infiniteScroll,
      initialImageCount: initialImageCount,
    );
  }

  @override
  _GalleryPageState createState() => _GalleryPageState();
}

class _GalleryPageState extends State<GalleryPage> with WidgetsBindingObserver {
  final GalleryPageController controller = Get.find<GalleryPageController>();

  @override
  void didChangeMetrics() {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      controller.handleOrientation();
    });
  }

  @override
  void initState() {
    controller.setInitialParameters(
      imageUrls: widget.imageUrls,
      infiniteScroll: widget.infiniteScroll,
      demoImageCount: widget.initialImageCount,
    );
    WidgetsBinding.instance?.addObserver(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors().scaffoldColor,
      appBar: GalleryAppBar(
        title: widget.title,
        actions: [
          Obx(() {
            return Visibility(
              visible: controller.showGridSwitch.value,
              child: InkWell(
                onTap: () {
                  controller.changeMode();
                },
                child: Obx(() {
                  return Icon(
                    controller.listMode.value ? Icons.grid_view : Icons.list,
                  );
                }),
              ),
            );
          }),
        ],
      ),
      body: Obx(() => GridView.builder(
            itemCount: controller.getItemCount(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: controller.count.value,
              childAspectRatio: controller.aspectRatio,
              crossAxisSpacing: gridSpace,
              mainAxisSpacing: gridSpace,
            ),
            itemBuilder: (context, index) => Hero(
              tag: "Tag$index",
              child: GalleryImageWidget(
                key: Key('$index'),
                imageUrl: controller.getImageUrl(index: index),
                itemClick: () {
                  Get.toNamed(
                    ImageViewerPage.routeName,
                    arguments: ImageViewerPageArguments(
                      imageUrl: controller.getFullScreenImageUrl(index: index),
                      heroTag: "Tag$index",
                      title: 'Image ($index)',
                    ),
                  );
                },
              ),
            ),
          )),
    );
  }
}
