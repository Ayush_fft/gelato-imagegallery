import 'package:flutter/material.dart';
import 'package:image_gallery/controllers/image_viewer_page_controller.dart';
import 'package:image_gallery/utils/colors.dart';
import 'package:image_gallery/utils/size_utils.dart';
import 'package:image_gallery/widgets/app_bar_widget.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';
import 'package:shimmer/shimmer.dart';

class ImageViewerPage extends GetView<ImageViewerPageController> {
  static const String routeName = '/image_viewer_page';

  const ImageViewerPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var args = Get.arguments as ImageViewerPageArguments?;

    return Scaffold(
      backgroundColor: CustomColors().scaffoldColor,
      appBar: GalleryAppBar(
        title: args?.title ?? "Image Viewer",
        actions: [
          InkWell(
            onTap: () {
              controller.shareImage(args?.imageUrl ?? "");
            },
            child: const Icon(
              Icons.share,
            ),
          ),
          InkWell(
            onTap: () {
              controller.saveImageToGallery(args?.imageUrl ?? "");
            },
            child: const Icon(
              Icons.save_alt_outlined,
            ),
          ),
        ],
      ),
      body: Stack(
        children: [
          Hero(
            tag: args?.heroTag ?? "",
            child: PhotoView(
              minScale: 0.4,
              imageProvider: NetworkImage(args?.imageUrl ?? ""),
              loadingBuilder: (context, event) => Shimmer.fromColors(
                baseColor: CustomColors().shimmerBaseColor,
                highlightColor: CustomColors().shimmerHighlightColor,
                child: Center(
                  child: Icon(
                    Icons.image,
                    size: 300.iY,
                    color: Colors.white,
                  ),
                ),
              ),
              errorBuilder: (context, error, stackTrace) => Center(
                child: Icon(
                  Icons.broken_image,
                  size: 300.iY,
                  color: CustomColors().shimmerBaseColor,
                ),
              ),
            ),
          ),
          Obx(() {
            return Visibility(
              visible: controller.loader.value,
              child: const AbsorbPointer(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            );
          }),
        ],
      ),
    );
  }
}

class ImageViewerPageArguments {
  final String imageUrl;
  final String heroTag;
  final String title;

  ImageViewerPageArguments({
    required this.imageUrl,
    required this.heroTag,
    required this.title,
  });
}
