import 'package:get/get.dart';
import 'package:image_gallery/controllers/image_viewer_page_controller.dart';
import 'package:image_gallery/services/download_image_service.dart';

class ImageViewerBindings implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DownloadImageService>(() => DownloadImageServiceImpl());
    Get.lazyPut(() => ImageViewerPageController(Get.find()));
  }
}
