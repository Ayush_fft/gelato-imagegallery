import 'package:get/get.dart';
import 'package:image_gallery/controllers/gallery_page_controller.dart';

class InitialBindings implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => GalleryPageController());
  }
}
