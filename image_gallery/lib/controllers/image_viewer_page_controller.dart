import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_gallery/services/download_image_service.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import '../utils/snackbar_utils.dart';

class ImageViewerPageController extends GetxController {
  var loader = false.obs;
  final DownloadImageService downloadService;

  ImageViewerPageController(this.downloadService);

  void saveImageToGallery(String imageUrl) async {
    loader.value = true;
    var response = await downloadService.downloadImage(imageUrl);
    response.fold(
      (l) {
        loader.value = false;
      },
      (r) async {
        await ImageGallerySaver.saveImage(
          Uint8List.fromList(r),
          quality: 100,
        );
        loader.value = false;
        showSnackBar("Image Saved Successfully");
      },
    );
  }

  Future<File?> writeFileToAppDirectory(String url) async {
    var response = await downloadService.downloadImage(url);

    return await response.fold(
      (l) {
        loader.value = false;

        return null;
      },
      (r) async {
        final Directory directory = await getApplicationDocumentsDirectory();
        final File file = File('${directory.path}/share.jpg');
        await file.writeAsBytes(r);
        loader.value = false;

        return file;
      },
    );
  }

  void shareImage(String imageUrl) async {
    loader.value = true;
    final box = Get.context!.findRenderObject() as RenderBox?;
    File? file = await writeFileToAppDirectory(imageUrl);
    if (file != null) {
      await Share.shareFiles(
        [file.path],
        sharePositionOrigin: box!.localToGlobal(Offset.zero) & box.size,
      );
    }
    loader.value = false;
  }
}
