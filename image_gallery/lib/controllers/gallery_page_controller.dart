import 'package:flutter/material.dart';
import 'package:image_gallery/utils/dimens.dart';
import 'package:get/get.dart';

class GalleryPageController extends GetxController {
  var count = portraitGridCount.obs;
  var aspectRatio = gridAspectRatio;
  var imageSize = gridImageSize;
  var listMode = false.obs;
  var currentOrientation = Orientation.portrait.obs;
  var selectedItems = List<RxInt>.empty(growable: true);
  var showGridSwitch = true.obs;
  late int totalImages;
  final baseUrl = "https://picsum.photos/";
  List<String>? imageUrl;
  bool infiniteScroll = true;

  void setInitialParameters({
    List<String>? imageUrls,
    required bool infiniteScroll,
    int? demoImageCount,
  }) {
    imageUrl = imageUrls;
    totalImages = imageUrl != null && imageUrl!.isNotEmpty
        ? imageUrl!.length
        : demoImageCount ?? 300;
    this.infiniteScroll = infiniteScroll;
  }

  @override
  void onInit() {
    ever(currentOrientation, (Orientation currentOrientation) {
      if (currentOrientation == Orientation.portrait) {
        changeToPortraitMode();
      } else {
        changeToLandscapeMode();
      }
    });
    currentOrientation.value = Get.mediaQuery.orientation;
    super.onInit();
  }

  void changeToListMode() {
    listMode.value = true;
    aspectRatio = listAspectRatio;
    imageSize = listImageSize;
    count.value = portraitListCount;
  }

  void changeToGridMode() {
    listMode.value = false;
    aspectRatio = gridAspectRatio;
    imageSize = gridImageSize;
    count.value = portraitGridCount;
  }

  void changeMode() {
    if (listMode.value) {
      changeToGridMode();
    } else {
      changeToListMode();
    }
  }

  void changeToLandscapeMode() {
    showGridSwitch.value = false;
    aspectRatio = gridAspectRatio;
    imageSize = gridImageSize;
    count.value = landscapeGridCount;
  }

  void changeToPortraitMode() {
    showGridSwitch.value = true;
    if (listMode.value) {
      changeToListMode();
    } else {
      changeToGridMode();
    }
  }

  void handleOrientation() {
    currentOrientation.value = Get.mediaQuery.orientation;
  }

  String getImageUrl({required int index}) {
    if (imageUrl != null) {
      return imageUrl![index % totalImages];
    }

    return '$baseUrl$imageSize?image=${index % totalImages}';
  }

  String getFullScreenImageUrl({required int index}) {
    if (imageUrl != null) {
      return imageUrl![index % totalImages];
    }

    return '$baseUrl$fullScreenImageSize?image=${index % totalImages}';
  }

  int? getItemCount() {
    if (!infiniteScroll) return totalImages;

    return null;
  }
}
