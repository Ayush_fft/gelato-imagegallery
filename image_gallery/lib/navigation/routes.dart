import 'package:get/get.dart';
import 'package:image_gallery/di/image_viewer_bindings.dart';
import 'package:image_gallery/pages/image_viewer_page.dart';

class Routes {
  static final routes = [
    GetPage(
      name: ImageViewerPage.routeName,
      page: () => const ImageViewerPage(),
      binding: ImageViewerBindings(),
    ),
  ];
}
