import 'dart:io';
import 'dart:math';
import 'package:flutter_test/flutter_test.dart';
import 'package:image_gallery/controllers/gallery_page_controller.dart';

void main() {
  setUpAll(() {
    HttpOverrides.global = null;
  });
  test('should always get image url', () {
    final controller = GalleryPageController();
    controller.setInitialParameters(infiniteScroll: true);
    for (int i = 0; i < 100; i++) {
      var number = Random().nextInt(10000);
      expect(controller.getImageUrl(index: number).isNotEmpty, true);
    }
  });
}
