import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:image_gallery/controllers/image_viewer_page_controller.dart';
import 'package:image_gallery/gallery_main.dart';
import 'package:image_gallery/pages/image_viewer_page.dart';
import 'package:image_gallery/services/download_image_service.dart';
import 'package:image_gallery/utils/size_utils.dart';
import 'package:image_gallery/widgets/app_bar_widget.dart';
import 'package:image_gallery/widgets/gallery_image_widget.dart';
import 'package:photo_view/photo_view.dart';
import 'package:shimmer/shimmer.dart';

void main() {
  setUpAll(() {
    initSizeUtils();
  });

  testWidgets(
    'GalleryPage has an Appbar with title Test, '
    'contains multiple GalleryImageWidget and '
    'GalleryImageWidget can be Tap',
    (WidgetTester tester) async {
      await tester.pumpWidget(GalleryMain(
        title: 'Test',
      ));

      // Verify that widget has Appbar with title Test.
      expect(find.byType(GalleryAppBar), findsOneWidget);
      expect(find.text("Test"), findsOneWidget);

      // Verify that widget has multiple GalleryImageWidgets.
      expect(find.byType(GalleryImageWidget), findsWidgets);

      // Verify GalleryImageWidgets can be tap.
      await tester.tap(find.byKey(const Key("0")));
    },
  );

  testWidgets(
    'GalleryImageWidget contains Shimmer on Icon as Placeholder and '
    'contains one CachedNetworkImage containing Image Widget',
    (WidgetTester tester) async {
      await tester.pumpWidget(GalleryImageWidget(
        imageUrl: "",
        itemClick: () {},
      ));

      // Verify that widget has Shimmer on Icon as Placeholder.
      expect(find.byType(Shimmer), findsOneWidget);
      expect(
        find.byIcon(Icons.image),
        findsOneWidget,
      );

      // Verify that widget has one CachedNetworkImage and Image Widget.
      expect(find.byType(CachedNetworkImage), findsOneWidget);
      expect(find.byType(Image), findsOneWidget);
    },
  );

  testWidgets(
    'ImageViewerPage has an Appbar with title Test and actions share and download '
    'contains Hero Animation, '
    'contains Shimmer on Icon as Placeholder and '
    'have one Photoview',
    (WidgetTester tester) async {
      Get.put<DownloadImageService>(DownloadImageServiceImpl());
      Get.put(ImageViewerPageController(Get.find()));
      await tester.pumpWidget(const GetMaterialApp(
        home: ImageViewerPage(),
      ));

      // Verify that widget has Appbar with title Image Viewer and actions
      // share and download.
      expect(find.byType(GalleryAppBar), findsOneWidget);
      expect(find.text("Image Viewer"), findsOneWidget);
      expect(
        find.byIcon(Icons.save_alt_outlined),
        findsOneWidget,
      );
      expect(
        find.byIcon(Icons.share),
        findsOneWidget,
      );

      // Verify that widget has Hero Animation.
      expect(find.byType(Hero), findsOneWidget);

      // Verify that widget has Shimmer on Icon as Placeholder.
      expect(find.byType(Shimmer), findsOneWidget);
      expect(
        find.byIcon(Icons.image),
        findsOneWidget,
      );

      // Verify that widget has one photoView
      expect(find.byType(PhotoView), findsOneWidget);
    },
  );
}
