# Gelato Assignment

A Flutter project for Gelato Assignment which displays a list of images as a gallery(thumbnail) and user can tap the thumbnail to see a full screen picture with the possibility to zoom in/zoom out.

##### This project works with both Android and IOS

## Feature Listing

- Shows random images Gallery from [Picsum Photos](https://picsum.photos/)
- Gallery can be display in Grid view(Portrait and Landscape) and List view(Portrait).
- All the gallery images cached
- User can Click any image and open its full resolution image.
- Full resolution image can be zoom and pan.
- Image can be downloaded to photos library.
- User can share image to any shareable Application.

## How to use

```dart
// Directly use gallery for picsum images
GalleryMain()

// All the parameters for Gallery
GalleryMain(
        infiniteScroll: true, // optional (Default value = true)
        initialImageCount: 500, // optional (Default value = 300)
        title: "Demo Gallery", // optional (Default value = "Gallery)
        imageUrls: const [   // optional (Default picsum photos)
          "https://picsum.photos/200?image=222",
          "https://picsum.photos/200?image=241",
          "https://picsum.photos/200?image=141",
          "https://picsum.photos/200?image=341",
          "https://picsum.photos/200?image=512",
        ],
      )
```

## ScreenShots

Android             |  IOS
:-------------------------:|:-------------------------:
<img src="screenshots/Android_1.png" width="250" />  |  <img src="screenshots/IOS_1.png" width="250" />
<img src="screenshots/Android_2.png" width="250" />  |  <img src="screenshots/IOS_2.png" width="250" />
<img src="screenshots/Android_3.png" width="250" />  |  <img src="screenshots/IOS_3.png" width="250" />

## Test Cases

#### Test Cases Done

- Should always get image url from controller.
- GalleryImageWidget TestWidget
- GalleryPage TestWidget
- ImageViewPage TestWidget

#### Test Cases Remaining

- Testing each network request for image downloading.
- Testing GalleryImageWidget Error Handling.

#### Note: Because of Time Constraints I have to use CachedNetworkImage(url) to show images in Gallery, which works efficiently in most of the scenarios. But if there is more specific requirement, I can create custom solution of downloading image by creating parallel network requests according to displayed images on screen and available ram size and Add those images into cache memory, For this custom approach we can follow clean architecture.